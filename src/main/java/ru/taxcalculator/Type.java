package ru.taxcalculator;

public enum Type {
    INCOME("Доход"),
    CONSUMPTION("Расход");

    private final String type;

    Type(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
