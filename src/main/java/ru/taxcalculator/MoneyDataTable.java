package ru.taxcalculator;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import org.hibernate.Session;
import ru.taxcalculator.dao.MoneyDataDao;
import ru.taxcalculator.entities.MoneyData;
import ru.taxcalculator.utils.HibernateUtil;

import java.util.List;

public class MoneyDataTable {
    private static TableView<MoneyData> moneyDataTable;

    @SafeVarargs
    private static void showData(TableColumn<MoneyData, ?>... columns) {
        clearTable();

        Session session = HibernateUtil.openSession();

        MoneyDataDao moneyDataDao = new MoneyDataDao(session);
        setItems(getObservableList(moneyDataDao.findAll()));

        session.close();

        addColumns(columns);
    }

    private static void clearTable() {
        moneyDataTable.getColumns().clear();
    }

    private static void setItems(ObservableList<MoneyData> observableList) {
        moneyDataTable.setItems(observableList);
    }

    private static ObservableList<MoneyData> getObservableList(List<MoneyData> moneyData) {
        return FXCollections.observableList(moneyData);
    }

    @SafeVarargs
    private static void addColumns(TableColumn<MoneyData, ?>... columns) {
        moneyDataTable.getColumns().addAll(columns);
    }

    public static void initializeTable(TableView<MoneyData> table) {
        moneyDataTable = table;
    }

    public static void showTableMoneyData() {
        TableColumn<MoneyData, Integer> number = new TableColumn<>("№");
        number.setCellValueFactory(new PropertyValueFactory<>("id"));
        number.setStyle("-fx-alignment: center");

        TableColumn<MoneyData, Double> amount = new TableColumn<>("Сумма");
        amount.setCellValueFactory(new PropertyValueFactory<>("amount"));

        TableColumn<MoneyData, Double> vatPercentage = new TableColumn<>("% НДС");
        vatPercentage.setCellValueFactory(new PropertyValueFactory<>("vatPercentage"));

        TableColumn<MoneyData, String> type = new TableColumn<>("Тип");
        type.setCellValueFactory(new PropertyValueFactory<>("type"));

        showData(number, amount, vatPercentage, type);
    }
}
