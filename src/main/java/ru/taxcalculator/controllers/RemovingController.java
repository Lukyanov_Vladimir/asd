package ru.taxcalculator.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import ru.taxcalculator.MoneyDataTable;
import ru.taxcalculator.dao.MoneyDataDao;
import ru.taxcalculator.utils.HibernateUtil;

public class RemovingController {
    @FXML
    private TextField id;

    @FXML
    private Button removeBtn;

    @FXML
    public void initialize() {
        removeBtn.setOnAction(event -> {
            if (!id.getText().equals("")) {
                Session session = HibernateUtil.openSession();
                MoneyDataDao moneyDataDao = new MoneyDataDao(session);
                moneyDataDao.delete(moneyDataDao.findById(Integer.parseInt(id.getText())));
                session.close();
                MoneyDataTable.showTableMoneyData();
                clearFields();
            }
        });
    }

    /**
     * Очищает поля
     */
    private void clearFields() {
        id.clear();
    }
}
