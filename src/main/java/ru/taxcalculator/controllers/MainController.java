package ru.taxcalculator.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import ru.taxcalculator.MoneyDataTable;
import ru.taxcalculator.entities.MoneyData;
import ru.taxcalculator.utils.HibernateUtil;

import java.io.IOException;

public class MainController {
    @FXML
    private TableView<MoneyData> table;

    @FXML
    private Button addBtn;

    @FXML
    private Button removeBtn;

    @FXML
    private Button editBtn;

    @FXML
    private Button calculateTaxBtn;

    private Stage primaryStage;

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @FXML
    public void initialize() {
        startHibernate();

        MoneyDataTable.initializeTable(table);
        MoneyDataTable.showTableMoneyData();

        addBtn.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/adding.fxml"));

                AddingController addingController = new AddingController();
                fxmlLoader.setController(addingController);

                Parent root = fxmlLoader.load();

                Stage stage = new Stage();
                Scene scene = new Scene(root);

                stage.setTitle("Добавление");
                stage.centerOnScreen();
                stage.setScene(scene);
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        removeBtn.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/removing.fxml"));

                RemovingController removingController = new RemovingController();
                fxmlLoader.setController(removingController);

                Parent root = fxmlLoader.load();

                Stage stage = new Stage();
                Scene scene = new Scene(root);

                stage.setTitle("Удаление");
                stage.centerOnScreen();
                stage.setScene(scene);
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        editBtn.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/editing.fxml"));

                EditingController editingController = new EditingController();
                fxmlLoader.setController(editingController);

                Parent root = fxmlLoader.load();

                Stage stage = new Stage();
                Scene scene = new Scene(root);

                stage.setTitle("Редактирование");
                stage.centerOnScreen();
                stage.setScene(scene);
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        calculateTaxBtn.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/report.fxml"));

                ReportController reportController = new ReportController();
                fxmlLoader.setController(reportController);

                Parent root = fxmlLoader.load();

                Stage stage = new Stage();
                Scene scene = new Scene(root);

                stage.setTitle("Налог на прибль");
                stage.centerOnScreen();
                stage.setScene(scene);
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Завершает работу программы
     */
    public void shutdown() {
        shutdownHibernate();
        closeApp();
    }

    /**
     * Закрывает главное окно программы
     */
    private void closeApp() {
        primaryStage.close();
    }

    /**
     * Cтартует hibernate
     */
    private void startHibernate() {
        HibernateUtil.buildSessionFactory();
    }

    /**
     * Останавливает hibernate
     */
    private void shutdownHibernate() {
        HibernateUtil.shutdown();
    }
}
