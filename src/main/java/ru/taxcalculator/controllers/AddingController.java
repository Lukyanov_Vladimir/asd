package ru.taxcalculator.controllers;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import ru.taxcalculator.MoneyDataTable;
import ru.taxcalculator.Type;
import ru.taxcalculator.dao.MoneyDataDao;
import ru.taxcalculator.entities.MoneyData;
import ru.taxcalculator.utils.HibernateUtil;

public class AddingController {
    @FXML
    private TextField amount;

    @FXML
    private TextField vatPercentage;

    @FXML
    private ComboBox<String> type;

    @FXML
    private Button addBtn;

    @FXML
    public void initialize() {
        type.setItems(FXCollections.observableArrayList(Type.CONSUMPTION.getType(), Type.INCOME.getType()));
        type.setValue(Type.INCOME.getType());

        addBtn.setOnAction(event -> {
            if (!amount.getText().equals("") && !vatPercentage.getText().equals("")) {
                Session session = HibernateUtil.openSession();
                MoneyDataDao moneyDataDao = new MoneyDataDao(session);
                moneyDataDao.save(createMoneyData());
                session.close();
                MoneyDataTable.showTableMoneyData();
                clearFields();
            }
        });
    }

    /**
     * Создаёт объект MoneyData
     *
     * @return - объект MoneyData
     */
    private MoneyData createMoneyData() {
        MoneyData moneyData = new MoneyData();
        moneyData.setAmount(Double.parseDouble(amount.getText()));
        moneyData.setVatPercentage(Double.parseDouble(vatPercentage.getText()));
        moneyData.setType(type.getValue());
        return moneyData;
    }

    /**
     * Очищает поля
     */
    private void clearFields() {
        amount.clear();
        vatPercentage.clear();
    }
}
