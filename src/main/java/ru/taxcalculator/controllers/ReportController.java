package ru.taxcalculator.controllers;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import ru.taxcalculator.dao.MoneyDataDao;
import ru.taxcalculator.entities.MoneyData;
import ru.taxcalculator.utils.HibernateUtil;

import java.util.List;

public class ReportController {

    @FXML
    private Label result1;

    @FXML
    private Label result2;

    @FXML
    private Label result3;

    @FXML
    private Label result4;

    @FXML
    private TextField taxAmount;

    @FXML
    private Button calcBtn;

    private double totalProfit;

    private double totalLoss;

    private double incomeTax;

    private double profitMinusExpense;

    public void initialize() {
        calcBtn.setOnAction(event -> {
            if (!taxAmount.getText().equals("")) {
                calcTotalProfitWithoutVat();
                calcTotalLossExcludingVat();
                calcIncomeTax();
                calcProfitLossAfterTaxes();
            }
        });
    }

    /**
     * Рассчитывает общую сумму прибыли без НДС
     */
    public void calcTotalProfitWithoutVat() {
        Session session = HibernateUtil.openSession();
        MoneyDataDao moneyDataDao = new MoneyDataDao(session);
        List<MoneyData> moneyDataList = moneyDataDao.sqlQuery("FROM MoneyData WHERE type='Доход'");

        double totalAmount = 0;

        for (MoneyData moneyData : moneyDataList) {
            totalAmount += moneyData.getAmount() / (moneyData.getVatPercentage() / 100 + 1);
        }

        totalProfit = totalAmount;

        result1.setText(String.valueOf(totalAmount));
    }

    /**
     * Рассчитывает общую сумму убытков без НДС
     */
    public void calcTotalLossExcludingVat() {
        Session session = HibernateUtil.openSession();
        MoneyDataDao moneyDataDao = new MoneyDataDao(session);
        List<MoneyData> moneyDataList = moneyDataDao.sqlQuery("FROM MoneyData WHERE type='Расход'");

        double totalAmount = 0;

        for (MoneyData moneyData : moneyDataList) {
            totalAmount += moneyData.getAmount() / (moneyData.getVatPercentage() / 100 + 1);
        }

        totalLoss = totalAmount;

        result2.setText(String.valueOf(totalAmount));
    }

    /**
     * Рассчитывает налог на прибыль
     */
    public void calcIncomeTax() {
        profitMinusExpense = totalProfit - totalLoss;

        incomeTax = profitMinusExpense * (Double.parseDouble(taxAmount.getText()) / 100);

        result3.setText(String.valueOf(incomeTax));
    }

    /**
     * Рассчитывает прибыль / убыток после налогов
     */
    public void calcProfitLossAfterTaxes() {
        result4.setText(String.valueOf(profitMinusExpense - incomeTax));
    }
}
