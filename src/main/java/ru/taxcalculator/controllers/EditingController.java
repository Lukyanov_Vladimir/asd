package ru.taxcalculator.controllers;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import org.hibernate.Session;
import ru.taxcalculator.MoneyDataTable;
import ru.taxcalculator.Type;
import ru.taxcalculator.dao.MoneyDataDao;
import ru.taxcalculator.entities.MoneyData;
import ru.taxcalculator.utils.HibernateUtil;

public class EditingController {
    @FXML
    private TextField id;

    @FXML
    private Button select;

    @FXML
    private TextField amount;

    @FXML
    private TextField vatPercentage;

    @FXML
    private ComboBox<String> type;

    @FXML
    private Button editBtn;

    private MoneyData moneyData;

    @FXML
    public void initialize() {
        type.setItems(FXCollections.observableArrayList(Type.CONSUMPTION.getType(), Type.INCOME.getType()));
        type.setValue(Type.INCOME.getType());

        disableFields(true);

        select.setOnAction(event -> {
            if (!id.getText().equals("")) {
                Session session = HibernateUtil.openSession();
                MoneyDataDao moneyDataDao = new MoneyDataDao(session);
                moneyData = moneyDataDao.findById(Integer.parseInt(id.getText()));
                session.close();

                amount.setText(moneyData.getAmount().toString());
                vatPercentage.setText(moneyData.getVatPercentage().toString());
                type.setValue(moneyData.getType());

                disableFields(false);
            }
        });

        editBtn.setOnAction(event -> {
            if (!id.getText().equals("") && !amount.getText().equals("") && !vatPercentage.getText().equals("")) {
                Session session = HibernateUtil.openSession();
                MoneyDataDao moneyDataDao = new MoneyDataDao(session);
                editMoneyData();
                moneyDataDao.update(moneyData);
                session.close();
                MoneyDataTable.showTableMoneyData();
                disableFields(true);
                clearFields();
            }
        });
    }

    /**
     * Заполняет новыми данными объект MoneyData
     */
    private void editMoneyData() {
        moneyData.setAmount(Double.parseDouble(amount.getText()));
        moneyData.setVatPercentage(Double.parseDouble(vatPercentage.getText()));
        moneyData.setType(type.getValue());
    }

    /**
     * Очищает поля
     */
    private void clearFields() {
        amount.clear();
        vatPercentage.clear();
        id.clear();
    }

    /**
     * Делает поля не активными
     *
     * @param bool - true or false
     */
    private void disableFields(boolean bool) {
        amount.setDisable(bool);
        vatPercentage.setDisable(bool);
        type.setDisable(bool);
    }
}
