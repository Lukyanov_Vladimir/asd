package ru.taxcalculator.main;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import ru.taxcalculator.controllers.MainController;

public class Main extends Application {

    private MainController mainController;
    private final EventHandler<WindowEvent> onCloseRequestAppBtnHandler = event -> mainController.shutdown();

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Запуск главного окна приложения
     *
     * @param primaryStage - главное окно
     * @throws Exception - ошибка
     */
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));

        mainController = new MainController();
        mainController.setPrimaryStage(primaryStage);
        fxmlLoader.setController(mainController);

        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root);

        primaryStage.setTitle("Калькулятор налога");
        primaryStage.centerOnScreen();
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(onCloseRequestAppBtnHandler);
        primaryStage.show();
    }
}
