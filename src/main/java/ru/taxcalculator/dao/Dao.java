package ru.taxcalculator.dao;

import java.util.List;

public interface Dao<T> {

    T findById(int id);

    List<T> findAll();

    List<T> sqlQuery(String query);

    void save(T essence);

    void update(T essence);

    void delete(T essence);
}
