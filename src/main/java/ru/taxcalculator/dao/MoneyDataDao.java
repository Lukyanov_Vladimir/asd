package ru.taxcalculator.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.taxcalculator.entities.MoneyData;

import java.util.List;

public class MoneyDataDao implements Dao<MoneyData>{

    private final Session session;

    public MoneyDataDao(Session session) {
        this.session = session;
    }

    /**
     * Ищет запись-объект по id
     *
     * @param id - id
     * @return - MoneyData
     */
    @Override
    public MoneyData findById(int id) {
        return session.get(MoneyData.class, id);
    }


    /**
     * Получает все записи-объекты из базы данных
     *
     * @return - список объектов MoneyData
     */
    @Override
    public List<MoneyData> findAll() {
        return (List<MoneyData>) session.createQuery("FROM MoneyData").list();
    }

    /**
     * Обрабатывает sql запрос
     *
     * @param query - sql запрос
     * @return - список с результатами
     */
    @Override
    public List<MoneyData> sqlQuery(String query) {
        return (List<MoneyData>) session.createQuery(query).list();
    }

    /**
     * Сохраняет объект MoneyData в Бд
     *
     * @param moneyData - объект, который нужно сохранить
     */
    @Override
    public void save(MoneyData moneyData) {
        Transaction tr = session.beginTransaction();
        session.save(moneyData);
        tr.commit();
    }

    /**
     * Обновляет данные объекта MoneyData в Бд
     *
     * @param moneyData - объект, который нужно обновить
     */
    @Override
    public void update(MoneyData moneyData) {
        Transaction tr = session.beginTransaction();
        session.update(moneyData);
        tr.commit();
    }

    /**
     * Удаляет данные объекта MoneyData в Бд
     *
     * @param moneyData - объект, который нужно удалить
     */
    @Override
    public void delete(MoneyData moneyData) {
        Transaction tr = session.beginTransaction();
        session.delete(moneyData);
        tr.commit();
    }
}
