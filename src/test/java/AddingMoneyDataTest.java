import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;
import ru.taxcalculator.dao.MoneyDataDao;
import ru.taxcalculator.entities.MoneyData;
import ru.taxcalculator.utils.HibernateUtil;

public class AddingMoneyDataTest {

    private final MoneyData actualMoneyData = createMoneyData();

    @Test
    public void testAddingRecordDatabase() {
        HibernateUtil.buildSessionFactory();
        Assert.assertTrue(save());
        HibernateUtil.shutdown();
    }

    private MoneyData createMoneyData() {
        MoneyData moneyData = new MoneyData();
        moneyData.setAmount(103d);
        moneyData.setVatPercentage(20d);
        moneyData.setType("Доход");
        return moneyData;
    }

    private boolean save() {
        Session session = HibernateUtil.openSession();
        MoneyDataDao moneyDataDao = new MoneyDataDao(session);
        moneyDataDao.save(actualMoneyData);
        MoneyData expectedMoneyData = moneyDataDao.findById(actualMoneyData.getId());
        session.close();
        return actualMoneyData.equals(expectedMoneyData);
    }
}
